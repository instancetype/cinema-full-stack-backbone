/**
 * Created by instancetype on 7/16/14.
 */
var _ = require('underscore')
  , Backbone = require('backbone')

var Monitor = function(collection) {
  _.extend(this, Backbone.Events)
  this.listenTo(collection, 'all', function(eventName) {
    console.log(eventName)
  })
}

module.exports = Monitor