/**
 * Created by instancetype on 7/17/14.
 */
var Backbone = require('backbone')
  , _ = require('underscore')
  , Movies = require('collections/movies')
  , data = require('../../../movies.json')
  , movies = new Movies(data)
  , MovieView = require('views/movie')
  , MoviesListView = require('views/moviesList')
  , Layout = require('views/layout')

var MoviesRouter = Backbone.Router.extend({

  routes: {
    'movies/:id': 'selectMovie'
  , '': 'showMain'
  }

, selectMovie: function(id) {
    this.movies.resetSelected()
    this.movies.selectByID(id)
    this.layout.setDetails(this.movies.get(id))
  }

, showMain: function() {
    this.movies.resetSelected()
    this.layout.setChose()
  }

, initialize: function(options) {
    this.movies = movies
    this.layout = Layout.getInstance({ el: '#main'
                                     , router: this
                                     })
    this.layout.render()
  }
})

module.exports = MoviesRouter