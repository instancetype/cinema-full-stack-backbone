/**
 * Created by instancetype on 7/15/14.
 */
var Backbone = require('backbone')
  , $ = require('jquery-untouched')
  , MoviesRouter = require('routers/movies')
Backbone.$ = $

//var Movies = require('collections/movies')
//  , data = require('../movies.json')
//  , Monitor = require('./monitor')
//  , MovieView = require('views/movie')
//  , MoviesListView = require('views/moviesList')
//
//var movies = new Movies(data)
//  , monitor = new Monitor(movies)
//
//module.exports = { movies: movies
//                 , MovieView: MovieView
//                 , MoviesListView: MoviesListView
//                 }

$(function() {
  var router = new MoviesRouter({el: $('#main')})
  Backbone.history.start({
    pushState: true,
    root: '/'
  })
})