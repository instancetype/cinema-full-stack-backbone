/**
 * Created by instancetype on 7/15/14.
 */
var Backbone = require('backbone')
  , Movie = require('models/movie')

var Movies = Backbone.Collection.extend({
  model: Movie

  // Unselect all
, resetSelected: function() {

    this.each(function(model) {
      model.set({'selected': false})
    })
  }

, selectByID: function(id) {

    this.resetSelected()
    var movie = this.get(id)
    movie.set({'selected': true})

    return movie.id
  }

, sortByTitle: function() {
    return this.sortBy('title')
  }

, sortByRating: function() {
    var sorted = this.sortBy(function(m) {
      return (10 - m.get('rating'))
    })
    return sorted
  }

, sortByShowtime: function() {
    return this.sortBy('showtime')
  }

, log: function() {
    console.log(this.models)
    this.each(function(movie) {
      console.log(movie.get('title') +
                    '  ' + movie.showtimeToString() +
                    '(' + movie.get('showtime') + ')')
    })
  }
})

module.exports = Movies