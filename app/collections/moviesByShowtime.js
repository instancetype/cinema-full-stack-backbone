/**
 * Created by instancetype on 7/21/14.
 */
var Backbone = require('backbone')
  , _ = require('underscore')
  , Movie = require('models/movie')

var MoviesByShowtime = Backbone.Collection.extend({
  model: Movie

, comparator: function(m) {
    return -m.toShowtimeDate()
  }

, sortByTitle: function() {
    return this.sortBy('title')
  }

, log: function() {
    console.log(this.models)
    this.each(function(movie) {
      console.log(movie.get('title') +
                    '  ' + movie.showtimeToString() +
                    '(' + movie.get('showtime') + ')')
    })
  }
})

module.exports = MoviesByShowtime