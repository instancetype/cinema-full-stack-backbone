/**
 * Created by instancetype on 7/18/14.
 */
var Backbone = require('backbone')
  , _ = require('underscore')

var ChoseView = Backbone.View.extend({

  template: _.template('<h1>Welcome to CINEMA</h1>' +
                       '<h2>Choose a movie!</h2>')

, className: 'details'

, render: function() {
    this.$el.html(this.template())

    return this
  }
})

module.exports = ChoseView