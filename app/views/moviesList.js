/**
 * Created by instancetype on 7/17/14.
 */
var Backbone = require('backbone')
  , MovieView = require('views/movie')

var MoviesListView = Backbone.View.extend({
  tagName: 'section'

, render: function(options) {
    var self = this
    var moviesListView = this.collection.map(function(movie) {
      return (new MovieView({ model: movie, router: self.router })).render().el
    })
    this.$el.html(moviesListView)

    return this
  }

, initialize: function(options) {
    this.router = options.router
  }

})

module.exports = MoviesListView