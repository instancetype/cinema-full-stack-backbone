/**
 * Created by instancetype on 7/17/14.
 */
var $ = require('jquery-untouched')
  , Backbone = require('backbone')
  , _ = require('underscore')

var MovieView = Backbone.View.extend({
  tagName: 'article'
, className: 'movie'
, template: '<h1><%= title %></h1>'

, initialize: function(options) {
    this.listenTo(this.model, 'change:title', this.render)
    this.listenTo(this.model, 'change:selected', this.render)
    this.router = options.router
  }

, render: function() {
    var tmpl = _.template(this.template)
    this.$el.html(tmpl(this.model.toJSON()))
    this.$el.toggleClass('selected', this.model.get('selected'))

    return this
  }

, events: {
    'click': '_selectMovie'
  }

, _selectMovie: function(e) {

    if (!this.model.get('selected')) {
      this.model.collection.resetSelected()
      this.model.collection.selectByID(this.model.id)
      this.router.navigate('/movies/' + this.model.id, {trigger: true})
    }
  }
})

module.exports = MovieView