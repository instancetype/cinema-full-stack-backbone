/**
 * Created by instancetype on 7/18/14.
 */
var Backbone = require('backbone')
  , _ = require('underscore')

var DetailsView = Backbone.View.extend({

  el: '#details'
, template: _.template('<h1><%= showtime %></h1><hr> <p><%= description %></p>')

, render: function() {
    this.$el.html(this.template(this.model.toJSON()))

    return this
  }
})

module.exports = DetailsView