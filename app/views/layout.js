/**
 * Created by instancetype on 7/18/14.
 */
var Backbone = require('backbone')
  , _ = require('underscore')
  , MoviesListView = require('views/moviesList')
  , DetailsView = require('views/details')
  , ChoseView = require('views/chose')

var Layout = Backbone.View.extend({

  template: _.template('<div id="overview"></div>' +
                       '<div id="details"></div>')

, render: function() {
    this.$el.html(this.template())
    this.currentDetails.setElement(this.$('#details')).render()
    this.overview.setElement(this.$('#overview')).render()

    return this
  }

, initialize: function(options) {
    this.overview = new MoviesListView({
      collection: options.router.movies
    , router: options.router
    })
    this.currentDetails = new ChoseView()
  }

, setDetails: function(movie) {
    if (this.currentDetails) this.currentDetails.remove()
    this.currentDetails = new DetailsView({model: movie})
    this.render()
  }

, setChose: function() {
    if (this.currentDetails) this.currentDetails.remove()
    this.currentDetails = new ChoseView()
    this.render()
  }
})

var instance

Layout.getInstance = function(options) {
  if (!instance) {
    instance = new Layout({
      el: options.el
    , router: options.router
    , collection: options.router.movies
    })
  }
  return instance
}

module.exports = Layout